#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(D7, D8); // RX, TX
ESP8266WiFiMulti WiFiMulti;
String connectserver = "http://192.168.1.16/getdata.php?id=";

char c=0;
char buffer1[40]= {};
int len=0;
const char* uid;

int stepesp=0;
int stepreadserial=0;
void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
  Serial.println();
  Serial.println();
  Serial.println();
  for (uint8_t t = 4; t > 0; t--) {
  Serial.printf("[SETUP] WAIT %d...\n", t);
  Serial.flush();
  delay(1000);
  }
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("MinhPhuc", "Tp-LiNk9999");
  Serial.printf("ok");
}

void loop1() {
  if(mySerial.available())
    {
      c=mySerial.read();
      Serial.print(c);
      if(c!=';')
      {
        buffer1[len]=c;
        len++;
        Serial.print(len,DEC);
      } else {
        len=0;
        stepreadserial =1;
      }
    }
}
void loop() {
  if(stepesp ==0)
  {
    readsserial();
  }
  if(stepesp ==1)
  {
    Serial.println("dayla16" );
    getdata();
  }
}

void readsserial()
{
  if(stepreadserial ==0)
  {
    if(Serial.available())
    {
      c=Serial.read();
      Serial.print(c);
      if(c!=';')
      {
        buffer1[len]=c;
        len++;
        Serial.print(" " );
      } else {
        len=0;
        Serial.println("dayla;" );
        stepreadserial =1;
        Serial.println("dayla1" );
        Serial.println(buffer1);
        Serial.print(stepreadserial,DEC);
        Serial.println("dayla12" );
      }
    }
  }
  if(stepreadserial ==1)
  {
    Serial.println("dayla13" );
    Serial.println(buffer1);
    Serial.println("dayla14" );
     StaticJsonDocument<200> doc;
     DeserializationError error = deserializeJson(doc, buffer1);
     Serial.print(buffer1);
     if (!error) 
     {
        JsonObject jsondata = doc.as<JsonObject>();
        uid = jsondata["id"];
        Serial.println("dayla15" );
        stepesp =1 ;
        Serial.println(stepesp,DEC);
        Serial.println(uid);
     }  else {
        //mySerial.print("loi;");
        stepesp =0;
        stepreadserial=0;
     }
  }
}

void getdata()
{
  // wait for WiFi connection
  if ((WiFiMulti.run() == WL_CONNECTED)) 
  {
    WiFiClient client;
    HTTPClient http;
    Serial.print("[HTTP] begin...\n");
    if (http.begin(client, (connectserver+uid)))   // HTTP
    {
      Serial.print("[HTTP] GET...\n");
      Serial.print(connectserver);
      // start connection and send HTTP header
      int httpCode = http.GET();
      // httpCode will be negative on error
      if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);
      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = http.getString();
          mySerial.print(payload);
          mySerial.print(";");
          Serial.println(payload);
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }
      http.end();
      stepesp =0;
    } else {
      Serial.printf("[HTTP} Unable to connect\n");
      stepesp =0;
    }
  } else {
    Serial.println("Loi ket noi WIFI ");
  }
  }
  stepesp =0;
}
