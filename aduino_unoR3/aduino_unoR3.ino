/*
Ket noi voi Arduino 
 * Pin layout should be as follows:
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
 * keypad1    A0            1 (chieu dau *)
 * keypad2    A1            2 (chieu dau *)
 * keypad3    A2            3 (chieu dau *)
 * keypad4    A3            4 (chieu dau *)
 * keypad5    4             5 (chieu dau *)
 * keypad6    5             6 (chieu dau *)
 * keypad7    6             7 (chieu dau *)
 * keypad8    7             8 (chieu dau *)
 * SDA_LCD    A4            SDA  (i2c lcd board) 
 * SCL_LCD    A5            SCD  (i2c lcd board)
 *   RX       2             RX                 D8 (nodeMCU)
 *   TX       3             TX                 D7 (nodeMCU)
 *   
 *   ServoPIN   8                 pin servo          
 *   
 */ 
/*

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);
#include <Keypad.h>
#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <MFRC522.h> // thu vien "RFID".
#include <Servo.h> 
Servo myservo;
#define SS_PIN 53
#define RST_PIN 5
#define ServoPIN 8
#define Quangtro 15
char hexaKeys[4][4] = {
  {'D','C','B','A'},
  {'#','9','6','3'},
  {'0','8','5','2'},
  {'*','7','4','1'}
};
byte rowPins[4] = {23,25, 27, 29}; //connect to the row pinouts of the keypad
byte colPins[4] = {31, 33, 35, 37}; //connect to the column pinouts of the keypad
//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, 4, 4); 
SoftwareSerial mySerial(10, 11); // RX, TX
MFRC522 mfrc522(SS_PIN, RST_PIN); 

unsigned long uidDec, uidDecTemp; // hien thi so UID dang thap phan
const char* ten;
const char* chucdanh;
const char* status1;
const char* thoigian;
char buffer[100]= {};
byte uidByte[20];
char c=0;
int len=0;
int lenuid=0;
int stepdiemdanh = 0;
int stepdocthe = 0;
int stepguikiemtrathe = 0;
int stepxulychuoi = 0;
int stepdiemdanhmaso = 0;
int statuscua =0;
unsigned long time1=0;
unsigned long timemocua=0;

unsigned long j=0;
void setup(){
  lcd.init();   // initializing the LCD
  SPI.begin(); // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522

  mySerial.begin(9600);
  Serial.begin(9600);
  myservo.attach(ServoPIN);
  pinMode(Quangtro, INPUT);
  lcd.backlight(); // Enable or Turn On the backlight 
  lcd.setCursor(0,0);
  lcd.print("    Diem Danh    "); // Start Printing
  delay(500);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("XIN MOI QUET THE");
  
}


void loop() {
  if(stepdiemdanh==0)
  {
    docthe();
  }
  if(stepdiemdanh==1)
  {
    kiemtrathe();
  }
  checkmocua();
}
void docthe1()
{
   //Serial.println("stepdocthe0");
  if(stepdocthe ==0)
  {
    char myKey = customKeypad.getKey();
    if (myKey == '*')
      {
        Serial.print("Key pressed: ");
        Serial.println(myKey);
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Nhap ma so:");
        lcd.setCursor(1,0);
        lcd.print(" ");
        uidDec=0;
        lenuid=0;
        stepdocthe=1;
        time1=millis();
      }
  }
  if(stepdocthe ==1)
  {
    char myKey = customKeypad.getKey();
    if (myKey != NULL)
    {
      if((myKey =='0')||(myKey =='1')||(myKey =='2')||(myKey =='3')||(myKey =='4')||(myKey =='5')||(myKey =='6')||(myKey =='7')||(myKey =='8')||(myKey =='9'))
      {
        lenuid++;
        Serial.print("Key pressed: ");
        Serial.println(myKey,DEC);
        uidDec=(uidDec*10)+(myKey-48);
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Nhap ma so:");
        lcd.setCursor(1,1);
        lcd.print(uidDec,DEC);
      }
      if(myKey=='*')
      {
        stepguikiemtrathe =0;
        stepdiemdanh =1;
        stepdocthe =0;
        Serial.print("So the:");
        Serial.println(uidDec,DEC);
        Serial.println(stepdiemdanh,DEC);
        Serial.println(stepguikiemtrathe,DEC);
        Serial.println(stepdocthe,DEC);
      }
    }
    if((millis()-time1)>10000)
    {
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("XIN MOI QUET THE");
        stepdocthe =0;
    }
  }
}
void docthe()
{
    if ( mfrc522.PICC_IsNewCardPresent()) {
        Serial.print("doc the:");
        if ( mfrc522.PICC_ReadCardSerial()) {  
          for (byte i = 0; i < mfrc522.uid.size; i++) {
            uidByte[i] = mfrc522.uid.uidByte[i];
            uidDec = uidDec*256+uidByte[i];
          } 
            stepdiemdanh=1;
            stepguikiemtrathe=0;
            Serial.print(stepdiemdanh,DEC);
            Serial.print("So the:");
            Serial.println(uidDec,DEC);
        }
    }
}

void kiemtrathe()
{
  //Serial.println("kiemtrathe0");
  if(stepguikiemtrathe ==0)
  {
    stepguikiemtrathe=1;
    mySerial.write("{\"id\":\"");
    Serial.write("{\"id\":\"");
    mySerial.print(uidDec,DEC);
    Serial.print(uidDec,DEC);
    mySerial.write("\"};");
    Serial.write("\"};");
    time1=millis();
  }
  if(stepguikiemtrathe==1)
  {
    if(mySerial.available())
    {
      c=mySerial.read();
      Serial.print(c);
      if(c!=';')
      {
        buffer[len]=c;
        len++;
      } else {
        buffer[len]=0;
        len=0;
        stepguikiemtrathe=2;
      }
    }
    if((millis()-time1)>10000)
    {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Ket Noi Bi Loi");
      delay(1000);
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("XIN MOI QUET THE");
      stepdiemdanh=0;
      stepguikiemtrathe=0;
    }
  }
  if(stepguikiemtrathe==2)
  {
    StaticJsonDocument<200> doc;
    Serial.println("giatri:");
    Serial.println(buffer);
     DeserializationError error = deserializeJson(doc, buffer);
     if (!error) 
     {
      Serial.println("e");
        JsonObject jsondata = doc.as<JsonObject>();
        ten = jsondata["T"];
        thoigian = jsondata["TG"];
        status1 = jsondata["S"];
        chucdanh = jsondata["CD"];
        stepguikiemtrathe=3 ;
     }  else {
        Serial.println("n");
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        stepguikiemtrathe=0; // quay ve buoc gui data
     }
  }
  if(stepguikiemtrathe==3)
  {
    hienthithe();
    delay(1000);
    stepguikiemtrathe=4;
    Serial.print("stepguikiemtrathe4");
  }
  if(stepguikiemtrathe==4)
  {
    Serial.print("stepguikiemtrathe5");
    if(status1[0]=='1')
    {
      Serial.println("Mo cua");
      statuscua=1; 
      Serial.println(statuscua,DEC);
      checkmocua();
      
    }
    if(chucdanh[0]=='G')
    {
      Serial.print("Giao vien");
      lcd.clear();
      lcd.setCursor(0,1);
      lcd.print("DUNG MS (*)");
      delay(500);
      stepguikiemtrathe=5;
       Serial.print("giao vien");
      time1=millis();
    }else {
      stepguikiemtrathe=0;
      stepdiemdanh=0;
      Serial.print("khong phai giao vien");
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("XIN MOI QUET THE");
    }
  }
  if(stepguikiemtrathe==5)
  {
    Serial.print("giao vien");
    char myKey = customKeypad.getKey();
    if (myKey == '*')
    {
      Serial.print("Key pressed: ");
      Serial.println(myKey);
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Nhap ma so:");
      uidDec=0;
      lenuid=0;
      stepguikiemtrathe=6;
    }
    if((millis()-time1)>2000)
    {
      stepguikiemtrathe==0;
      stepdiemdanh=0;
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("XIN MOI QUET THE");
    }
  }
  if(stepguikiemtrathe==6)
  {
    char myKey = customKeypad.getKey();
    if (myKey != NULL)
    {
      if((myKey =='0')||(myKey =='1')||(myKey =='2')||(myKey =='3')||(myKey =='4')||(myKey =='5')||(myKey =='6')||(myKey =='7')||(myKey =='8')||(myKey =='9'))
      {
        lenuid++;
        Serial.print("Key pressed: ");
        Serial.println(myKey,DEC);
        uidDec=(uidDec*10)+(myKey-48);
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Nhap ma so:");
        lcd.setCursor(1,1);
        lcd.print(uidDec,DEC);
      }
      if(myKey=='*')
      {
        stepguikiemtrathe =0;
        stepdiemdanh =1;
        stepdocthe =0;
        Serial.print("So the:");
        Serial.println(uidDec,DEC);
        Serial.println(stepdiemdanh,DEC);
        Serial.println(stepguikiemtrathe,DEC);
        Serial.println(stepdocthe,DEC);
      }
    }
    if((millis()-time1)>10000)
    {
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("XIN MOI QUET THE");
        stepdocthe =0;
    }
  }
}


void hienthithe()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(ten);
  lcd.setCursor(13,0);
  lcd.print(chucdanh);
  lcd.setCursor(0,1);
  lcd.print(uidDec);
  Serial.print("ten:");
  Serial.println(ten);
  Serial.print("chucdanh:");
  Serial.println(chucdanh);
  Serial.print("uidDec:");
  Serial.println(uidDec);
}

void mocua()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("    MOI VAO!   ");
  lcd.setCursor(0,1);
  lcd.print(" Trong 3 giây  ");
  myservo.write(90);
  delay(3000);
  myservo.write(0);
}

void checkmocua()
{
  if(statuscua==1)  // kiem tra bat dau mo cua 
  {
    Serial.println("mo cua trong 3s");
    myservo.write(90);
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("    MOI VAO!   ");
    lcd.setCursor(0,1);
    lcd.print(" Trong 3 giây  ");
    timemocua=millis();
    statuscua=2;  
  }
  if(statuscua==2)  // kiem tra dang mo cua
  {
    
    if( (millis() - timemocua) > 3000)   // mo trong 3s
    {
      if(!digitalRead(Quangtro))  // neu quang tro =0 khong co chuyen dong
      {
        statuscua=3;          // chuyen sang buoc dong cua
        Serial.println("kiem tra do quang tro");
      }
    }
  }
  if(statuscua==3)       // dong cua 
  {
    Serial.println("dong cua");
    statuscua=0;
    myservo.write(0);
  }
}



