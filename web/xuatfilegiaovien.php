<?php
require "Classes/PHPExcel.php";
	$username = "root"; // Khai báo username
	$password = "";      // Khai báo password
	$server   = "localhost";   // Khai báo server
	$dbname   = "diemdanh";      // Khai báo database
	$connect = new mysqli($server, $username, $password, $dbname);
	if ($connect->connect_error) {
	die("Không kết nối :" . $conn->connect_error);
	exit();
	}


//Khởi tạo đối tượng
$excel = new PHPExcel();
//Chọn trang cần ghi (là số từ 0->n)
$excel->setActiveSheetIndex(0);
//Tạo tiêu đề cho trang. (có thể không cần)
$excel->getActiveSheet()->setTitle('diem danh giang vien');

//Xét chiều rộng cho từng, nếu muốn set height thì dùng setRowHeight()
$excel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
$excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
$excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
//Xét in đậm cho khoảng cột
$excel->getActiveSheet()->getStyle('B3:H3')->getFont()->setBold(true);
$excel->getActiveSheet()->mergeCells('B1:H2');
$excel->getActiveSheet()->getStyle('B3:H3')->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
																			'startcolor' => array('rgb' => '2F72C3')));

$excel->getActiveSheet()->setCellValue('B1', 'Điểm Danh Giao Vien');
$excel->getActiveSheet()->setCellValue('B3', 'STT');
$excel->getActiveSheet()->setCellValue('C3', 'Tên');
$excel->getActiveSheet()->setCellValue('D3', 'Mã Thẻ');
$excel->getActiveSheet()->setCellValue('E3', 'Mã Số');
$excel->getActiveSheet()->setCellValue('F3', 'Tuần');
$excel->getActiveSheet()->setCellValue('G3', 'Thời Gian');
$excel->getActiveSheet()->setCellValue('H3', 'Ghi chú');
// thực hiện thêm dữ liệu vào từng ô bằng vòng lặp
// dòng bắt đầu = 2
$numRow = 4;
$sql1 = "SELECT * FROM diemdanh.dlgiaovien order by STT DESC LIMIT 1000";
	$result = $connect->query($sql1);
	if ($result->num_rows >0) {
		// output data of each row
		While ($row = $result->fetch_assoc()) {
			$stt=$row["STT"];
			$ten=$row["TEN"];
			$id=$row["ID"];
			$maso=$row["MASO"];
			$tuan=$row["TUAN"];
			$thoigian=$row["THOIGIAN"];
			$ghichu=$row["NOTE"];
			$excel->getActiveSheet()->setCellValue('B' . $numRow, $stt);
			$excel->getActiveSheet()->setCellValue('C' . $numRow, $ten);
			$excel->getActiveSheet()->setCellValue('D' . $numRow, $id);
			$excel->getActiveSheet()->setCellValue('E' . $numRow, $maso);
			$excel->getActiveSheet()->setCellValue('F' . $numRow, $tuan);
			$excel->getActiveSheet()->setCellValue('G' . $numRow, $thoigian);
			$excel->getActiveSheet()->setCellValue('H' . $numRow, $ghichu);
			$numRow++;
		}
	}
// Khởi tạo đối tượng PHPExcel_IOFactory để thực hiện ghi file
// ở đây mình lưu file dưới dạng excel2007
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="diemdanhgiaovien.xls"');
PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save('php://output');
$_SERVER['HTTP_REFERER']
?>