-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: diemdanh
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dlgiaovien`
--

DROP TABLE IF EXISTS `dlgiaovien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dlgiaovien` (
  `STT` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ID` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MASO` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TUAN` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `THOIGIAN` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTE` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`STT`),
  UNIQUE KEY `STT_UNIQUE` (`STT`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dlgiaovien`
--

LOCK TABLES `dlgiaovien` WRITE;
/*!40000 ALTER TABLE `dlgiaovien` DISABLE KEYS */;
INSERT INTO `dlgiaovien` VALUES (1,'Nguyen Quan','11111','11111','T7','2019-01-12 11:44:31',NULL),(2,'Nguyen Quan','11111','11111','T7','2019-01-12 11:44:39',NULL),(3,'Nguyen Quan','11111','11111','02','2019-01-12 11:56:08',NULL),(4,'Nguyen Quan','11111','11111','02','2019-01-12 12:16:14',''),(5,'Nguyen Quan','11111','11111','02','2019-01-12 12:17:16',''),(6,'Nguyen Quan','11111','11111','02','2019-01-12 12:17:37',''),(7,'Nguyen Quan','11111','11111','02','2019-01-12 12:17:38',''),(8,'Nguyen Quan','11111','11111','02','2019-01-12 12:17:38',''),(9,'Nguyen Quan','11111','11111','02','2019-01-12 12:19:15','NOTE'),(10,'Nguyen Quan','11111','11111','02','2019-01-12 12:19:17','NOTE'),(11,'Nguyen Quan','11111','11111','02','2019-01-12 12:19:19','NOTE'),(12,'Nguyen Quan','11111','11111','02','2019-01-12 12:19:20','NOTE');
/*!40000 ALTER TABLE `dlgiaovien` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-12 12:46:19
