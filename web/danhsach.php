<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>
<table id="customers">
<tr><td><center><h3>Danh sách thẻ đã được gán</h3></center></td></tr>
</table>
<br>
<table id="customers">
  <tr>
    <th>STT</th>
    <th>Tên</th>
    <th>Mã Thẻ</th>
	<th>Mã Số</th>
	<th>Chức danh</th>
  </tr>
<?php
	$username = "root"; // Khai báo username
	$password = "";      // Khai báo password
	$server   = "localhost";   // Khai báo server
	$dbname   = "diemdanh";      // Khai báo database
	$connect = new mysqli($server, $username, $password, $dbname);
	if ($connect->connect_error) {
	die("Không kết nối :" . $conn->connect_error);
	exit();
	}
	$sql1 = "SELECT * FROM diemdanh.danhsach ";
	$result = $connect->query($sql1);
	if ($result->num_rows >0) {
		// output data of each row
		While ($row = $result->fetch_assoc()) {
			$tenchucdanh="Giáo Viên";
			if($row["CHUCDANH"]=="HOCSINH"){
				$tenchucdanh="Học Sinh";
			}
			$stt=$row["STT"];
			$ten=$row["TEN"];
			$id=$row["ID"];
			$maso=$row["MASO"];
			echo "<tr><td> $stt </td><td> $ten </td><td> $id </td><td> $maso </td><td> $tenchucdanh </td></tr>";
		}
	}
?>
</table>

</body>
</html>
